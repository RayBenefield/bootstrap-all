indentation="    "

indentation="    "

function Indent()
{
    indentation="${indentation}    "
}

function Unindent()
{
    indentation="${indentation:0:${#indentation}-4}"
}

function PrintInfo()
{
    info=${1}
    echo -e "${indentation}${Green}» ${info}${No_Color}"
}

function PrintBoldInfo()
{
    info=${1}
    echo -e "${indentation}${BGreen}» ${info}${No_Color}"
}

function PrintCodeLine()
{
    code=${1}
    echo -e "${indentation}    ${Cyan}${code}${No_Color}"
}

function PrintDescription()
{
    description=${1}
    echo -e "${indentation}    ${description}"
}

function PrintWarning()
{
    warning=${1}
    echo -e "${indentation}${BYellow}» ${warning}${No_Color}" | tr -d '\n'
    echo
}

function PrintSeparator()
{
    echo -e "    ${Purple}--------------------------------------------------${No_Color}"
}

function PrintPasta()
{
    pasta=${1}
    echo -e "${pasta}"
}

function PrintError()
{
    error=${1}
    echo -e "${indentation}${Red}» ${White}${On_Red}[ERROR]${Red} ${error}${No_Color}"
}

function PrintException()
{
    Indent
    PrintWarning "${@}"
    Unindent
}
